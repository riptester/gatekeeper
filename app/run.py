import asyncio
import logging
import os
import struct
import tempfile
from asyncio import IncompleteReadError

import aiodocker.stream

from .utils import read_var_string, read_int, read_var_string_array, CloseableStreamReader, \
    read_chunked_string

logger = logging.getLogger(__name__)


async def create_container(
        docker: aiodocker.Docker,
        image_name: str,
        command: list[bytes],
        environment: list[bytes],
        stop_timeout: int,
        tmpfs_size: int,
        memory_limit: int,
        pids_limit: int,
        cpu_period: int,
        cpu_quota: int,
        code_file: str
) -> aiodocker.docker.DockerContainer:
    if ":" in code_file:
        raise RuntimeError(f"A colon ended up in code file path {code_file}")

    docker_config = {
            "Image": image_name,
            "Cmd": [part.decode("latin-1") for part in command],
            "Env": [entry.decode("latin-1") for entry in environment],
            "AttachStdin": True,
            "AttachStdout": True,
            "AttachStderr": True,
            "OpenStdin": True,
            "WorkingDir": "/tmp",
            "NetworkDisabled": True,
            "StopTimeout": stop_timeout,
            "HostConfig": {
                "Mounts": [
                    {
                        "Target": "/tmp",
                        "Type": "tmpfs",
                        "TmpfsOptions": {
                            "SizeBytes": tmpfs_size
                        }
                    }
                ],
                "Memory": memory_limit,
                "PidsLimit": pids_limit,
                "CpuPeriod": cpu_period,
                "CpuQuota": cpu_quota,
                "CapDrop": "ALL",
                "Binds": [f"{code_file}:/code:Z"],
                "ReadonlyRootfs": True,
                "LogConfig": {"Type": "none"}
            },
        }
    try:
        container = await docker.containers.create(docker_config)
    except aiodocker.DockerError as e:
        if not e.status == 404:
            raise
        await docker.pull(docker, image_name)
        container = await docker.containers.create(docker_config)
    return container


async def run_container(
        reader: CloseableStreamReader,
        writer: asyncio.StreamWriter,
        session: int,
        docker: aiodocker.Docker
):
    # noinspection PyBroadException
    try:
        image_name = await read_var_string(reader)
        command = await read_var_string_array(reader)
        environment = await read_var_string_array(reader)
        exec_time = await read_int(reader)
        stop_timeout = await read_int(reader)
        tmpfs_size = await read_int(reader)
        memory_limit = await read_int(reader)
        pids_limit = await read_int(reader)
        cpu_period = await read_int(reader)
        cpu_quota = await read_int(reader)
        code = read_chunked_string(reader)
        with tempfile.NamedTemporaryFile(dir="./code_files", delete=False) as code_file:
            code_path = os.path.abspath(code_file.name)
            async for chunk in code:
                code_file.write(chunk)
        container = await create_container(
            docker,
            image_name.decode("utf-8"),
            command,
            environment,
            stop_timeout,
            tmpfs_size,
            memory_limit,
            pids_limit,
            cpu_period,
            cpu_quota,
            code_path
        )
        in_stream = container.attach(stdin=True)
        out_stream = container.attach(stdout=True, stderr=True)
        asyncio.create_task(stop_after_timeout(exec_time, container))
        await container.start()
        asyncio.create_task(copy_input(reader, in_stream))
        await copy_output(session, out_stream, writer)
        await container.wait()
        data = await container.show()
        rc = data["State"]["ExitCode"]
        start = data["State"]["StartedAt"].encode("latin-1")
        end = data["State"]["FinishedAt"].encode("latin-1")
        writer.write(struct.pack("!IIbhI", session, 7 + len(start) + len(end), -1, rc, len(start)) + start + end)
    except Exception:
        logging.exception("Failure while running container")
    finally:
        reader.close()
        try:
            os.remove(code_path)
        except FileNotFoundError:
            pass


async def copy_input(reader: asyncio.StreamReader, in_stream: aiodocker.stream.Stream):
    async with in_stream:
        try:
            while True:
                data = await read_var_string(reader)
                await in_stream.write_in(data)
        except IncompleteReadError:
            # stream is dead, no more stdin
            pass


async def copy_output(session: int, out_stream: aiodocker.stream.Stream, writer: asyncio.StreamWriter):
    async with out_stream:
        while message := await out_stream.read_out():
            writer.write(struct.pack("!IIb", session, 1 + len(message.data), message.stream) + message.data)


async def stop_after_timeout(timeout: int, container: aiodocker.docker.DockerContainer):
    await asyncio.sleep(timeout)
    await container.stop()
