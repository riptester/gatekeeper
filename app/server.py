import asyncio
import logging
import struct

import aiodocker

from .run import run_container
from .utils import CloseableStreamReader


logger = logging.getLogger(__file__)


class Server:
    """
    Handle the gatekeeper protocol
    Every packet begins with 4 bytes of session ID followed by 4 bytes of length
    A session ID cannot be zero, if zero is sent then all sessions will be deleted
    If the session ID is zero and data is provided, only the session ID given in data is deleted
    Each container gets a new session ID
    To send a string, send the length in bytes followed by the bytes
    To send a list, send the length in bytes followed by the serialization of all sub-things
    In a new session, the following data must be sent in order:
    - image_name
    - command
    - environment
    - exec_time
    - stop_timeout
    - tmpfs_size
    - memory_limit
    - pids_limit
    - cpu_period
    - cpu_quota
    Next, the input code is streamed to the gatekeeper.
    Inside each packet is a chunk of code (any length).
    If an empty packet is received it ends the code input.
    After these have been received you can send stdin data, it will be fed into the container as received
    The gatekeeper will reply with variable length packets.
    They always start with 4 bytes of container ID.
    Next, the length of the packet
    After the ID comes the message type (one signed byte):
    - 1, 2 = stdout or stderr content (without a length specified - infer it from the packet length)
    - -1 = container exit status (2 unsigned bytes of return code, and a double (8 bytes) of runtime)
    """
    tasks: dict[int, asyncio.Task]
    readers: dict[int, CloseableStreamReader]
    docker: aiodocker.Docker

    def __init__(self):
        self.tasks = {}
        self.readers = {}
        self.docker = aiodocker.Docker("unix:///run/user/1000/podman/podman.sock")

    async def pump(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        while True:
            session, length = struct.unpack("!II", await reader.readexactly(8))
            if session == 0:
                if length == 0:
                    for session, task in self.tasks.items():
                        task.cancel()
                        del self.tasks[session]
                    for session_reader in self.readers.values():
                        session_reader.close()
                    await reader.readexactly(length)
                elif length == 4:
                    session, = struct.unpack("!I", await reader.readexactly(4))
                    try:
                        self.tasks.pop(session).cancel()
                        self.readers[session].close()
                    except KeyError:
                        pass

                    def closed():
                        del self.readers[session]

                    session_reader = CloseableStreamReader(closed)
                    self.readers[session] = session_reader
                    self.tasks[session] = asyncio.create_task(
                        run_container(
                            session_reader,
                            writer,
                            session,
                            self.docker
                        )
                    )
                continue
            try:
                session_reader = self.readers[session]
            except KeyError:
                logger.warning("Client responded to unknown session %d with length %d", session, length)
                continue
            session_reader.feed_data(await reader.readexactly(length))


async def main():
    server_class = Server()
    server = await asyncio.start_server(server_class.pump, '127.0.0.1', 8081)
    async with server:
        await server.serve_forever()
