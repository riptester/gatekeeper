import asyncio
import struct
from collections import AsyncIterable


async def read_var_string(reader: asyncio.StreamReader) -> bytes:
    length = await read_int(reader)
    return await reader.readexactly(length)


async def read_chunked_string(reader: asyncio.StreamReader) -> AsyncIterable[bytes]:
    while True:
        chunk = await read_var_string(reader)
        if not chunk:
            return
        yield chunk


async def read_var_string_array(reader: asyncio.StreamReader) -> list[bytes]:
    length = await read_int(reader)
    return [await read_var_string(reader) for _ in range(length)]


async def read_int(reader: asyncio.StreamReader) -> int:
    return struct.unpack("!I", await reader.readexactly(4))[0]


class CloseableStreamReader(asyncio.StreamReader):
    def __init__(self, closed, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._closed = closed

    def close(self):
        self.feed_eof()
        self._closed()
